(function ($) {
  Drupal.behaviors.field_optimizer = {
    attach: function (context, settings) {
      // Hide all but one variant.
      var variant_containers = $('div.field-optimizer-enabled div.field-items');
      var variants = [];

      var cookie = $.parseJSON($.cookie("Drupal.visitor.field_optimizer"));
      if (cookie) {
        var path = window.location.pathname.replace(/^\//g, '');
        if (cookie[path]) {
          var existingVariants = cookie[path]['variants'];
        }
      }

      // Check if we've met a goal.
      for (var page in cookie) {
        console.log(page);
        var goal = cookie[page]['goal'];
        console.log(goal);
        if (goal != null && goal['url'] == path) {
          console.log('GOAL FULFILLED!!!');
          // @todo - Record variants for this success.
        }
      }

      variant_containers.each(function() {
        // Get the field name.
        var field = $(this).closest('.field-optimizer-enabled');
        var classes = field[0].className.split(" ");
        var element = $.grep(classes, function(n, i) {
          return n.indexOf('field-name') === 0;
        });
        var fieldname = element[0];

        console.log(existingVariants);
        // Hide a variant.
        if (typeof existingVariants !== "undefined" && existingVariants[fieldname]) {
          var variant = existingVariants[fieldname];
        }
        else {
          var variant = Math.floor(Math.random() * $(this).children('div.field-item').size());
        }
        $('div.field-item:', this).hide().eq(variant).show();

        // Store the variant shown.
        var obj = {"fieldname": fieldname, "variant": variant};
        variants.push(obj);
      });
console.log(variants);
      if (variants.length > 0) {
        console.log('saving variants');
        var data = {
          "variants": variants,
          "path": window.location.pathname
        };
console.log('window.location.href: ' + window.location.href);
        // Send the variants shown to the server.
        // Could avoid sending ajax request but new fields wouldn't be saved.
        $.ajax({
          url: Drupal.settings.basePath + 'field-optimizer/ajax',
          type: 'POST',
          dataType: 'json',
          data: data,
          success: function(returnData) {
            var responseData = $.parseJSON(returnData[1]['data']);
            if (responseData['response'] == 'success') {
              console.log('Field optimizer success.');
            }
            else {
              console.log('Field optimizer failure.');
            }
          }
        });
      }
    }
  };
})(jQuery);
