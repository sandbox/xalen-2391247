Set up node references for source and goal pages.
Create JavaScript to randomly display variants and store variant shown in cookie.
Report on source vs goals.

Requirements
============

 - field_formatter_settings module
 - entity_reference module

Installation
============

 - Place jquery cookie in libraries/jquery-cookie (https://github.com/carhartl/jquery-cookie)
 - Copy module to sites/all/modules/[contrib]
 - Enable module

Usage
=====

 - Enable content type(s) for Field optimizer.
 - Make the fields you want to test multivalue fields.
 - Add a goal field of type "link" with a machine name of goal (field_goal).
   (Optional) Set the Link Title to "No Title".
 - Enable Field optimizer in field display.
 - (Optional) Hide the goal field and label.
 - Create a new node with field variants and goal page(s).
 - Test.
 - View statistics.

FAQ
===

Why the relation module instead of references or entity reference?
------------------------------------------------------------------

The references module will be deprecated in favour of entity reference module
which does everything the references module does and more.

The relation module uses entities to define the relation which makes it possible
for more information to be stored with the relation. For testing purposes this
allows us to add our own identifier so we can group variants together, e.g.
making all call to action buttons on all pages orange.

The relation module is also small with no further dependencies (Entity
reference depends on both the Entity API and CTools) and I didn't want to
add another two dependencies. In the future, I may add the ability to choose
between Entity reference and Relation since it is likely most sites will have
Entity API and CTools installed anyway.